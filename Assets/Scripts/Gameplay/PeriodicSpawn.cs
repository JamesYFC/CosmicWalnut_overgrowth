﻿using UnityEngine;
using System.Collections;

public class PeriodicSpawn : MonoBehaviour
{

    public int maxCount;
    private int count;

    public GameObject prefab;
    public string tagString;
    //get these right for the arena dimensions!!!
    public Vector2 spawnValues;
    public float spawnWait;
    public float startWait;

    void Start()
    {
        StartCoroutine(SpawnWaves());
    }

    void Update()
    {
        count = countObjects();
    }

    IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(startWait);
        while (true)
        {
            if (count < maxCount)
            {
                InstantiateNewObject();
            }
            yield return new WaitForSeconds(spawnWait);
        }
    }

    void InstantiateNewObject()
    {
        int tries = 10;
        Vector2 positionToInstantiate = new Vector2(Random.Range(-spawnValues.x, spawnValues.x), Random.Range(-spawnValues.y, spawnValues.y)); ;
        Collider2D[] hitColliders;
        float radius = prefab.GetComponent<CircleCollider2D>().radius * prefab.transform.localScale.magnitude;
        //Debug.Log("RADIUS " + radius);
        do
        {
            //Debug.Log(tries);
            positionToInstantiate = new Vector2(Random.Range(-spawnValues.x, spawnValues.x), Random.Range(-spawnValues.y, spawnValues.y)); ;
            hitColliders = Physics2D.OverlapCircleAll(positionToInstantiate, radius);

            --tries;
        } while (hitColliders.Length > 0 && tries > 0);

        if (tries > 0)
            Instantiate(prefab, positionToInstantiate, Quaternion.identity);

    }

    private int countObjects()
    {
        GameObject[] _objectsList = GameObject.FindGameObjectsWithTag(tagString);
        return _objectsList.Length;
    }
}
