﻿using UnityEngine;
using System.Collections;

public class PickupHelper 
{
    public static Pickup GetClosestPickup(Vector2 position, float radius)
    {
        Collider2D[] objects = Physics2D.OverlapCircleAll(position, radius);
        float closestDistance = float.MaxValue;
        float tempDistance = 0;
        int closestIndex = -1;

        for (int i = 0; i < objects.Length; i++)
        {
            Pickup p = objects[i].GetComponent<Pickup>();

            if (p != null)
            {
                if (!p.CanBePickedUp)
                {
                    continue;//Break out and check next item
                }

                tempDistance = Vector2.SqrMagnitude((Vector2)objects[i].bounds.center - position);
                if (tempDistance < closestDistance)
                {
                    closestIndex = i;
                }
            }
        }

        if (closestIndex >= 0)
        {
            return objects[closestIndex].GetComponent<Pickup>();
        }
        return null;
    }

}
