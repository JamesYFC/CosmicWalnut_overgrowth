﻿using UnityEngine;
using System.Collections;

public class Health {

    public int maxHealth { get;private set; }
    public int currentHealth { get; private set; }

    public Health(int maxHealth)
    {
        this.maxHealth = maxHealth;
        currentHealth = maxHealth;
    }

    //bool shows death or not
    public bool Damage(int dmg)
    {
        if (dmg < 0)
        {
            throw new System.Exception("dmg can't be lower than 0");
        }
        if (currentHealth - dmg <= 0)
        {
            currentHealth = 0;
            return true;
        }
        else
        {
            currentHealth -= dmg;
            return false;
        }
    }

    public void Heal(int heal)
    {
        if (heal < 0)
        {
            throw new System.Exception("heal can't be lower than 0");
        }
        if (currentHealth + heal > maxHealth)
        {
            currentHealth = maxHealth;
        }
        else
        {
            currentHealth += heal;
        }
        return;
    }

    public void SetHealth(int hp)
    {
        currentHealth = hp;
    }
}
