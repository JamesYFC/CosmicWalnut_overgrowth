﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MonsterMovement))]
public class MonsterAnimator : MonoBehaviour
{
    [SerializeField] private SpriteRenderer _sideSprite;
    [SerializeField] private SpriteRenderer _frontSprite;
    [SerializeField] private SpriteRenderer _backSprite;

    [SerializeField]
    private SpriteRenderer _attackingRenderer;

    [SerializeField] private Sprite _sideAttack;
    [SerializeField] private Sprite _frontAttack;
    [SerializeField] private Sprite _backAttack;


    [SerializeField]
    private bool _reverseSideSprite = false;

    [SerializeField]
    private Transform _target;

    private bool _isAttacking = false;

    public bool IsAttacking
    {
        get { return _isAttacking; }
        set { _isAttacking = value; }
    }

    MonsterMovement _movement;

    private Vector3 previousPosition;

    private void SetVisibleSprite(Vector2 direction)
    {
        if (_isAttacking)
        {
            SetAttackingSprite(direction);
            return;
        }
        _attackingRenderer.enabled = false;

        if (direction.sqrMagnitude < 0.05)
        {
            return;
        }
        if (Mathf.Abs(direction.x) > Mathf.Abs(direction.y))
        {
            _sideSprite.enabled = true;
            _frontSprite.enabled = false;
            _backSprite.enabled = false;

            _sideSprite.flipX = direction.x < 0;
            if (_reverseSideSprite)
            {
                _sideSprite.flipX = !_sideSprite.flipX;
            }
        }
        else
        {
            _sideSprite.enabled = false;
            if (direction.y > 0)
            {
                _frontSprite.enabled = false;
                _backSprite.enabled = true;
            }
            else
            {
                _frontSprite.enabled = true;
                _backSprite.enabled = false;
            }
        }
    }

    private void SetAttackingSprite(Vector2 direction)
    {
        _attackingRenderer.enabled = true;
        _sideSprite.enabled = false;
        _frontSprite.enabled = false;
        _backSprite.enabled = false;

        if (direction.sqrMagnitude< 0.05)
        {
            return;
        }

        if (Mathf.Abs(direction.x) > Mathf.Abs(direction.y))
        {
            _attackingRenderer.sprite = _sideAttack;

            _attackingRenderer.flipX = direction.x < 0;
            if (_reverseSideSprite)
            {
                _attackingRenderer.flipX = !_sideSprite.flipX;
            }
        }
        else
        {
            if (direction.y > 0)
            {
                _attackingRenderer.sprite = _backAttack;
            }
            else
            {
                _attackingRenderer.sprite = _frontAttack;
            }
        }

    }

    private void Start()
    {
        _movement = GetComponent<MonsterMovement>();
    }

    void FixedUpdate ()
    {
        Vector3 currentPosition = transform.position;
        if (_target != null)
        {
            SetVisibleSprite((_target.position - currentPosition).normalized);
        }
        else
        {
            SetVisibleSprite((currentPosition - previousPosition).normalized);
        }
 
        previousPosition = currentPosition;
    }

    



    private void Update()
    {
        _isAttacking = _movement.IsPouncing;
    }

}
