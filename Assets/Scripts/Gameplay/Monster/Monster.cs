﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MonsterMovement))]
public class Monster : MonoBehaviour
{
    public enum MonsterState
    {
        Idle,
        Chasing,
        Eating
    }

    private MonsterState _currentState;

    public MonsterState CurrentState
    {
        get { return _currentState; }
    }


    private void Awake()
    {

    }


    private void OnCollisionEnter2D(Collision2D col)
    {
        Pickup pickup = col.gameObject.GetComponent<Pickup>();
        if (pickup != null)
        {
            //Do logic here with pickup


        }
    }

}
