﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MonsterMovement : MonoBehaviour
{
    public string monsterType;
    [SerializeField]
    private float pounceThreshold = 0.2f;
    public List<GameObject> players;
    private Dictionary<GameObject, bool> pounceHits;
    private int targetIndex;
    private int friendIndex = -1;
    //private bool angry = false;
    private int angryIndex = -1;
    private ParticleSystem loveParticles;
    private CooldownManager _angryTime;
    public SpriteRenderer[] mySpriteRends;
    [SerializeField]
    private float speedRageMultiplier;
    private float currentTopSpeed;
    [SerializeField]
    private float pounceRange;
    [SerializeField]
    private float topSpeed;
    [SerializeField]
    private float accel;
    [SerializeField]
    private float pouncePower;
    public int pounceDamage;

    private Rigidbody2D myRigid;
    private bool Stunned;
    private bool pouncing;
    [SerializeField]
    private float pounceWindup;
    private bool pounceWinding;
    [SerializeField]
    private float radius;
    private bool pounced;


    public bool IsPouncing
    {
        get { return pounced; }
    }

    private Transform chargeDirection;

    [SerializeField]
    private float deccel;
    float multiply = 0;
    private MonsterSounds soundPlayer;

    void Awake()
    {
        _angryTime = gameObject.GetComponent<CooldownManager>();
        //Debug.Log(_angryTime.name);
        myRigid = gameObject.GetComponent<Rigidbody2D>();
        soundPlayer = gameObject.GetComponent<MonsterSounds>();
        loveParticles = gameObject.GetComponentInChildren<ParticleSystem>();
    }
    void Start()
    {
        currentTopSpeed = topSpeed;
        GetPlayers();
    }

    void FixedUpdate()
    {
        FindNearestPlayer();

        if (pounceWinding)
        {
            Decelerate();
        }

        if (!pouncing && !pounceWinding)
        {
            if (WithinPounceRange())
            {
                // Debug.Log("pounce");
                StartCoroutine(Pounce());
            }
            else
            {
                PursueTarget();
            }
        }

        if (pouncing && !pounceWinding)
        {
            if (myRigid.velocity.magnitude <= pounceThreshold && pounced)
            {
                ResetAttackBools();
                pouncing = false;
                pounced = false;
            }
        }

        if (pouncing && myRigid.velocity.magnitude > pounceThreshold && !pounced)
        {
            pounced = true;
        }
    }

    void Update()
    {
        if (!_angryTime.GetFirstStatus())
        {
            if (_angryTime.CooldownCheck())
            {
                if (angryIndex > -1)
                {
                    Debug.Log("not angry anymore!");
                    ResetAngry();
                }
            }
        }

        if (HasAngry() && !isFlash)
        {
            StartCoroutine(FlashRed());
        }
    }

    public void ResetAngry()
    {
        currentTopSpeed = topSpeed;
        angryIndex = -1;
    }

    private bool isFlash;
    IEnumerator FlashRed()
    {
        isFlash = true;
        foreach (SpriteRenderer sprtrend in mySpriteRends)
        {
            sprtrend.color = new Color(255, 0, 0);
        }
        yield return new WaitForSeconds(0.25f);
        foreach (SpriteRenderer sprtrend in mySpriteRends)
        {
            sprtrend.color = new Color(255, 255, 255);
        }
        yield return new WaitForSeconds(0.25f);
        isFlash = false;
    }

    public bool IsFriend(GameObject player)
    {
        if (friendIndex == -1)
        {
            return false;
        }
        else
        {
            if (players.IndexOf(player) == friendIndex)
            {
                return true;
            }
            return false;
        }
    }

    public bool IsAngry(GameObject player)
    {
        if (angryIndex == -1)
        {
            return false;
        }
        else
        {
            if (players.IndexOf(player) == angryIndex)
            {
                return true;
            }
            return false;
        }
    }

    public bool HasFriend()
    {
        if (friendIndex > -1)
        {
            return true;
        }
        return false;
    }
    public bool HasAngry()
    {
        if (angryIndex > -1)
        {
            return true;
        }
        return false;
    }

    public void MakeAngry(GameObject player)
    {
        if (players.Contains(player))
        {
            //if player to be angry at is friend, reset friend index
            angryIndex = players.IndexOf(player);
            if (friendIndex == angryIndex)
            {
                friendIndex = -1;
            }
            _angryTime.CheckFirst();
            _angryTime.SetCooldown();
            Debug.Log(gameObject.name + " is angry at " + player.name + "!");
            currentTopSpeed = topSpeed * 5;
        }
    }

    public int MakeFriend(GameObject player)
    {
        if (players.Contains(player))
        {
            friendIndex = players.IndexOf(player);
            FindNearestPlayer(friendIndex);
            Debug.Log(gameObject.name + "made friends with " + player.name + "index " + this.friendIndex + ", now new target is " + players[targetIndex].name);
            loveParticles.Emit(1);
            return friendIndex;
        }
        else return -1;
    }

    public void KillPlayerResponse(GameObject player)
    {
        if (players.Contains(player))
        {
            FindNearestPlayerKill(player);
        }
        return;
    }

    private void FindNearestPlayer()
    {
        if (angryIndex > -1)
        {
            targetIndex = angryIndex;
        }
        else
        {
            for (int i = 0; i < players.Count; i++)
            {
                if (i != targetIndex && i != friendIndex)
                {
                    if (Vector2.Distance(this.transform.position, players[i].transform.position) < Vector2.Distance(this.transform.position, players[targetIndex].transform.position))
                    {
                        targetIndex = i;
                    }
                }
            }
        }
    }

    private void FindNearestPlayer(int exceptedIndex)
    {
        List<GameObject> playersNew = new List<GameObject>();
        for (int i = 0; i < players.Count; i++)
        {
            playersNew.Add(players[i]);
        }
        playersNew.RemoveAt(exceptedIndex);

        int currentNearestIndex = 0;
        float maxDistance = Vector2.Distance(this.transform.position, playersNew[0].transform.position);
        for (int i = 0; i < playersNew.Count; i++)
        {
            if (Vector2.Distance(this.transform.position, players[currentNearestIndex].transform.position) < maxDistance)
            {
                currentNearestIndex = i;
            }
        }
        targetIndex = players.IndexOf(playersNew[currentNearestIndex]);
        return;
    }

    private void FindNearestPlayerKill(GameObject deadPlayer)
    {


        List<GameObject> playersNew = new List<GameObject>();
        for (int i = 0; i < players.Count; i++)
        {
            playersNew.Add(players[i]);
        }
        if (players.IndexOf(deadPlayer) == friendIndex)
        {
            Debug.Log("friend died! resetting index...");
            friendIndex = -1;
        }
        if (players.IndexOf(deadPlayer) == angryIndex)
        {
            Debug.Log("angrytarget died! resetting index...");
            ResetAngry();
        }
        playersNew.Remove(deadPlayer);

        if (angryIndex > -1 && players.IndexOf(deadPlayer) != angryIndex)
        {
            Debug.Log("someone died but I'm still gonna target the guy im angry at...");
            targetIndex = playersNew.IndexOf(players[angryIndex]);
        }
        else
        {
            //get nearest from new
            int currentNearestIndex = 0;
            float maxDistance = Vector2.Distance(this.transform.position, playersNew[0].transform.position);
            for (int i = 0; i < playersNew.Count; i++)
            {
                if (Vector2.Distance(this.transform.position, playersNew[currentNearestIndex].transform.position) < maxDistance)
                {
                    currentNearestIndex = i;
                }
            }
            players = playersNew;
            targetIndex = currentNearestIndex;
        }
        Debug.Log("players size is now " + players.Count + "\nand new targetIndex is " + targetIndex + "\nand current friendIndex is " + friendIndex);


    }

    private void GetPlayers()
    {
        players = new List<GameObject>();
        pounceHits = new Dictionary<GameObject, bool>();
        GameObject[] playersArr = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject p in playersArr)
        {
            players.Add(p);
            pounceHits.Add(p, false);
        }
    }

    private void ResetAttackBools()
    {
        pounceHits = new Dictionary<GameObject, bool>();
        foreach (GameObject p in players)
        {
            pounceHits.Add(p, false);
        }
    }

    private void PursueTarget()
    {
        if (multiply < 1)
        {
            multiply += accel;
        }
        else
        {
            multiply = 1;
        }
        transform.position = Vector2.MoveTowards(gameObject.transform.position, players[targetIndex].transform.position, currentTopSpeed * multiply);
    }

    private bool WithinPounceRange()
    {
        if (Vector2.Distance(this.transform.position, players[targetIndex].transform.position) <= pounceRange)
        {
            return true;
        }
        return false;
    }

    private void Decelerate()
    {
        if (multiply > 0)
        {
            multiply -= deccel;
        }
        else
        {
            multiply = 0;
        }
        transform.position = Vector2.MoveTowards(gameObject.transform.position, players[targetIndex].transform.position + Vector3.Normalize(this.transform.position - players[targetIndex].transform.position) * radius, currentTopSpeed * multiply);
    }

    IEnumerator Pounce()
    {
        Vector3 offset = players[targetIndex].transform.position - this.transform.position;
        Vector3 direction = offset.normalized; //The direction vector towards the player
        pounceWinding = true;
        soundPlayer.PlayWindupSound();
        yield return new WaitForSeconds(pounceWindup);
        pounceWinding = false;
        pouncing = true;
        soundPlayer.PlayPounceSound();
        myRigid.AddForce(direction * pouncePower);
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (pouncing == true)
        {
            if (col.gameObject.tag.Contains("Player") && !pounceHits[col.gameObject])
            {
                Player colPlayer = col.gameObject.GetComponent<Player>();
                pounceHits[col.gameObject] = true;
                colPlayer.Damage(pounceDamage);
                soundPlayer.PlayHitSound();
            }
        }
    }
}
