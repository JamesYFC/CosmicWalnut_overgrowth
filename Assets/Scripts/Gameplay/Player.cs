﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
[DisallowMultipleComponent]
public class Player : MonoBehaviour
{
    [SerializeField]
    private float _maxVelocity = 10f;
    [SerializeField]
    private float _acceleration = 5f;

    [SerializeField]
    private float _throwForce = 20f;

    public Health health;
    [SerializeField]
    private int initialHealth = 100;

    [SerializeField]
    private float _dashVelocity = 10f;

    [SerializeField]
    private float _dashDuration = 0.5f;

    [SerializeField]
    private Pickup _currentPickup = null;

    public CooldownManager _dashCooldown;
    private bool _isDashing = false;
    private Vector2 _targetVelocity;
    private AbstractInput _movement;
    private Rigidbody2D _rigidBody;
    private Collider2D _collider;
    private Vector2 _faceDirection = Vector2.right;


    private LevelManager _levelManager;

    public bool HasPickup
    {
        get { return _currentPickup != null; }
    }

    public int Damage(int dmg)
    {
        if (health.Damage(dmg))
        {
            _levelManager.ConfirmPlayerKill(gameObject);
            gameObject.SetActive(false);
            return 0;
        }
        else
        {
            return health.currentHealth;
        }
    }
    private void PickupObject()
    {
        if (!HasPickup)
        {
            float pickupRadius = 1.1f;
            Pickup p = PickupHelper.GetClosestPickup(_rigidBody.position, pickupRadius);
            if (p != null)
            {
                _currentPickup = p;
                _currentPickup.HandlePickup(this.transform, new Vector3(0, 1.2f, transform.position.z+2), _collider);
            }
        }
    }

    private void ThrowObject()
    {
        if (HasPickup)
        {
            //TODO- Add some throwing action
            _currentPickup.Throw(_throwForce * _faceDirection);
            _currentPickup.thrower = gameObject;
            //_currentPickup.HandleRealease();
            _currentPickup = null;
        }
    }

    private void ThrowObject(Vector2 direction)
    {
        if (HasPickup)
        {
            //TODO- Add some throwing action
            _currentPickup.Throw(_throwForce * direction);
            _currentPickup.thrower = gameObject;
            //_currentPickup.HandleRealease();
            _currentPickup = null;
        }
    }


    void Awake()
    {
        health = new Health(initialHealth);
        _rigidBody = GetComponent<Rigidbody2D>();
        _movement = GetComponent<AbstractInput>();
        _collider = GetComponent<Collider2D>();
        _dashCooldown = GetComponent<CooldownManager>();
        _levelManager = GameObject.FindGameObjectWithTag("LevelManager").GetComponent<LevelManager>();
    }

    void Start()
    {

    }

    void Update()
    {
        Dash();

        Grab();
    }

    private void Grab()
    {
        if (_movement.GrabButton)
        {
            if (HasPickup)
            {
                ThrowObject();
            }
            else
            {
                PickupObject();
            }
        }
        else if (_movement.AimingDirection.magnitude > 0.9f)
        {
            if (HasPickup)
            {
                ThrowObject(_movement.AimingDirection.normalized);
            }

        }
    }

    private void Dash()
    {
        if (_movement.DashButton && !_isDashing)
        {
            if (_dashCooldown.CooldownCheck())
            {
                _dashCooldown.CheckFirst();
                ActivateDash();
                _dashCooldown.SetCooldown();
            }
        }
        else
        {
            SetDashingTargetVelocity();
        }
    }

    private void SetDashingTargetVelocity()
    {
        _targetVelocity.x = _maxVelocity * _movement.Movement.x;
        _targetVelocity.y = _maxVelocity * _movement.Movement.y;
    }

    private void ActivateDash()
    {
        if (_movement.Movement.sqrMagnitude > 0.05f)
        {
            _isDashing = true;
            _rigidBody.velocity = _faceDirection * _dashVelocity;
            StartCoroutine(DashRoutine());
        }
    }

    private IEnumerator DashRoutine()
    {
        float _timeElapsed = 0;
        while (_timeElapsed < _dashDuration)
        {
            _timeElapsed += Time.deltaTime;
            yield return null;
        }
        _isDashing = false;
    }


    void FixedUpdate()
    {
        if (!_isDashing)
        {
            _rigidBody.velocity = Vector2.MoveTowards(_rigidBody.velocity, _targetVelocity, _acceleration * Time.fixedDeltaTime);
        }
        else
        {

        }


        //Update the face direction only when the player is moving
        if (_rigidBody.velocity.sqrMagnitude > 0.5f)
        {
            _faceDirection = _rigidBody.velocity.normalized;
        }

    }
}
