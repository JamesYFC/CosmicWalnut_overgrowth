﻿using UnityEngine;
using System.Collections;

public class PlayerAnimator : MonoBehaviour
{

    [SerializeField] private SpriteRenderer _sideIdleSprite;
    [SerializeField] private SpriteRenderer _frontIdleSprite;
    [SerializeField] private SpriteRenderer _backIdleSprite;

    [SerializeField] private SpriteRenderer _sideRunSprite;
    [SerializeField] private SpriteRenderer _frontRunSprite;
    [SerializeField] private SpriteRenderer _backRunSprite;

    [SerializeField]
    private bool _reverseSideSprite = false;
    private Vector3 previousPosition;

    private enum Direction
    {
        front,
        back,
        left,
        right
    }

    private Direction _currentDirection = Direction.front;



    public void SetPlayerOutLineColour(Color col)
    {
        _sideIdleSprite.material.color = col;
        _frontIdleSprite.material.color = col;
        _backIdleSprite.material.color = col;
        _sideRunSprite.material.color = col;
        _frontRunSprite.material.color = col;
        _backRunSprite.material.color = col;
    }



    private void SetVisibleSprite(Vector2 direction)
    {
        
        if (direction.magnitude == 0)
        {
            _sideRunSprite.enabled = false;
            _frontRunSprite.enabled = false;
            _backRunSprite.enabled = false;


            switch (_currentDirection)
            {
                case Direction.left:
                    _sideIdleSprite.enabled = true;
                    _frontIdleSprite.enabled = false;
                    _backIdleSprite.enabled = false;

                    _sideIdleSprite.flipX = _reverseSideSprite ? false : true;
                    break;
                case Direction.right:
                    _sideIdleSprite.enabled = true;
                    _frontIdleSprite.enabled = false;
                    _backIdleSprite.enabled = false;

                    _sideIdleSprite.flipX = _reverseSideSprite ? true : false;
                    break;
                case Direction.back:
                    _sideIdleSprite.enabled = false;
                    _frontIdleSprite.enabled = false;
                    _backIdleSprite.enabled = true;
                    break;
                case Direction.front:
                    _sideIdleSprite.enabled = false;
                    _frontIdleSprite.enabled = true;
                    _backIdleSprite.enabled = false;
                    break;
            }
            return;
        }
        

        _sideIdleSprite.enabled = false;
        _frontIdleSprite.enabled = false;
        _backIdleSprite.enabled = false;

        if (Mathf.Abs(direction.x) > Mathf.Abs(direction.y))
        {
            _sideRunSprite.enabled = true;
            _frontRunSprite.enabled = false;
            _backRunSprite.enabled = false;

            _sideRunSprite.flipX = direction.x < 0;
            _currentDirection = direction.x < 0 ? Direction.left : Direction.right;

            if (_reverseSideSprite)
            {
                _sideRunSprite.flipX = !_sideRunSprite.flipX;
            }
        }
        else
        {
            

            _sideRunSprite.enabled = false;
            if (direction.y > 0)
            {
                _frontRunSprite.enabled = false;
                _backRunSprite.enabled = true;

                _currentDirection = Direction.back;
            }
            else
            {
                _frontRunSprite.enabled = true;
                _backRunSprite.enabled = false;
                _currentDirection = Direction.front;
            }
            
        }
    }


    void Start()
    {
        previousPosition = transform.position;
        _currentDirection = Direction.front;
    }


	void FixedUpdate()
    {
        Vector3 currentPosition = transform.position;
        SetVisibleSprite((currentPosition - previousPosition).normalized);
        previousPosition = currentPosition;
    }
}
