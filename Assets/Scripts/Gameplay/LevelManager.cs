﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

    public List<GameObject> MonsterList;
    public List<GameObject> PlayerList;

    // Use this for initialization
    void Start()
    {
        MonsterList = new List<GameObject>();
        GameObject[] tempArray = GameObject.FindGameObjectsWithTag("Monster");
        for (int i = 0; i < tempArray.Length; i++)
        {
            MonsterList.Add(tempArray[i]);
        }

        PlayerList = new List<GameObject>();
        GameObject[] playerTempArray = GameObject.FindGameObjectsWithTag("Player");
        for (int j = 0; j < playerTempArray.Length; j++)
        {
            PlayerList.Add(playerTempArray[j]);
        }


    }

    // Update is called once per frame
    void Update()
    {
        foreach(GameObject mon in MonsterList)
        {
            mon.transform.position = new Vector3(mon.transform.position.x, mon.transform.position.y, mon.transform.position.y);
        }

        foreach (GameObject pla in PlayerList)
        {
            pla.transform.position = new Vector3(pla.transform.position.x, pla.transform.position.y, pla.transform.position.y);
        }

    }

    public void ConfirmPlayerKill(GameObject player)
    {

        Debug.Log(PlayerList.Count);
        if (PlayerList.Count == 2)
        {
            //a player wins
            Debug.Log("a player has won");
            PlayerList.Remove(player);
            SceneManager.LoadScene("PlayerSetupScreen");
            return;
        }
        else
        {
            foreach (GameObject monster in MonsterList)
            {
                monster.GetComponent<MonsterMovement>().KillPlayerResponse(player);
            }
            player.SetActive(false);
        }
    }
}
