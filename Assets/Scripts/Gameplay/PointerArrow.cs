﻿using UnityEngine;
using System.Collections;

public class PointerArrow : MonoBehaviour
{
    private AbstractInput _input;
    private float offset = 0.5f;

    void Start()
    {
        _input = GetComponentInParent<AbstractInput>();
    }


    // Update is called once per frame
    void Update ()
    {
        Vector3 direction = _input.AimingDirection.normalized;
        transform.up = direction;
        transform.localPosition = direction * offset;

    }
}
