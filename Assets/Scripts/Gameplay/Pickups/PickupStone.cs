﻿using UnityEngine;
using System.Collections;

public class PickupStone : Pickup
{
    protected override void InteractMonster(Collision2D col)
    {
        if (col.gameObject.tag.Contains("Monster"))
        {
            MonsterMovement monsterInteract = col.gameObject.GetComponent<MonsterMovement>();

            //if right kind
            if (monsterInteract.monsterType == "large")
            {
                //if not friend and not angry, become friend
                if (!monsterInteract.IsFriend(thrower) && !monsterInteract.HasAngry())
                {
                    Debug.Log("monsterName: " + col.gameObject.name + "\n thrower: " + thrower);
                    monsterInteract.MakeFriend(thrower);
                    Destroy(gameObject);
                }
                //otherwise it just bounces off
            }
            //if wrong kind, regardless of friend or angry
            else
            {
                ApplyForce(col);
                Destroy(gameObject);
            }


        }

    }

    private void ApplyForce(Collision2D col)
    {
        //make angry at them regardless of friend status (reset friend index if friend is done in monster)
        Debug.Log("monsterName: " + col.gameObject.name + "\n thrower: " + thrower);
        // force is how forcefully we will push the player away from the enemy.
        float force = 2000;
        // Calculate Angle Between the collision point and the player
        Vector2 dir = (Vector2)col.transform.position - (Vector2)transform.position;
        // We then get the opposite (-Vector3) and normalize it
        dir = dir.normalized;
        // And finally we add force in the direction of dir and multiply it by force. 
        // This will push back the player
        col.gameObject.GetComponent<Rigidbody2D>().AddForce(dir * force);
    }
}
