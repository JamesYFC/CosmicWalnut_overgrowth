﻿using UnityEngine;
using System.Collections;

public class PickupMeat : Pickup
{

    protected override void InteractMonster(Collision2D col)
    {
        if (col.gameObject.tag.Contains("Monster"))
        {
            MonsterMovement monsterInteract = col.gameObject.GetComponent<MonsterMovement>();
            if (!monsterInteract.HasAngry())
            {
                //if right kind
                if (monsterInteract.monsterType == "medium")
                {
                    //if not friend, become friend
                    if (!monsterInteract.IsFriend(thrower))
                    {
                        Debug.Log("monsterName: " + col.gameObject.name + "\n thrower: " + thrower);
                        monsterInteract.MakeFriend(thrower);
                        Destroy(gameObject);
                    }
                    //otherwise it just bounces off
                }
                //if wrong kind
                else
                {
                    //make angry at them regardless of friend status (reset friend index if friend is done in monster)
                    monsterInteract.MakeAngry(thrower);
                    Destroy(gameObject);
                }

            }
        }

    }
}
