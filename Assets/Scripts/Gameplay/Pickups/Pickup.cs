﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class Pickup : MonoBehaviour
{
    public GameObject thrower;
    private Rigidbody2D _rb2d;
    private Collider2D _collider;
    private Transform _transformCache = null;
    private Transform _parentTransform = null;
    private Collider2D _parentCollider = null;
    private bool throwing = false;

    // Use this for initialization
    void Start()
    {
        _rb2d = GetComponent<Rigidbody2D>();
        _collider = GetComponent<Collider2D>();
        _transformCache = transform;

    }

    // Update is called once per frame
    void Update()
    {
        //SetClosest();

        //if (thrower != null)
        //{
        //    Debug.Log(thrower.name);
        //}
        //else Debug.Log("No thrower");

    }

    void FixedUpdate()
    {
        CheckThrowingStatus();
    }

    private void CheckThrowingStatus()
    {
        if (_rb2d.velocity.sqrMagnitude < 1.8f && throwing == true)
        {
            //Debug.Log("throwing false");
            //_rb2d.velocity = new Vector2(0,0);
            throwing = false;
            thrower = null;
        }
    }

    public bool CanBePickedUp
    {
       get { return _parentTransform == null; }
    }



    public void HandlePickup(Transform parent, Vector3 offset, Collider2D parentCollider)
    {
        if (CanBePickedUp)
        {
            _transformCache.SetParent(parent);
            _parentTransform = parent;
            //TODO - Do some fancy transformation stuff
            _transformCache.localPosition = offset;
            _collider.isTrigger = true;
            _rb2d.isKinematic = true;
            _collider.isTrigger = true;
            _parentCollider = parentCollider;
            Physics2D.IgnoreCollision(_collider, parentCollider, true);
        }
    }

    public void HandleRealease()
    {
        if (_parentTransform != null)
        {
            _transformCache.SetParent(_parentTransform.parent);
            _parentTransform = null;
           
        }
    }

    public void Throw(Vector2 force)
    {
        if (_parentTransform != null)
        {
            //Debug.Log("throwing true");
            throwing = true;
            _transformCache.SetParent(_parentTransform.parent);
            _parentTransform = null;
            _rb2d.isKinematic = false;
            _collider.isTrigger = false;
            _rb2d.AddForce(force,ForceMode2D.Impulse);
        }
    }

    private void ResetIgnoreCollisions()
    {
        //After throwing a pickup, we want to to prevent the player from being able to interact with the pickup
        if (_parentCollider != null)
        {
            Physics2D.IgnoreCollision(_collider, _parentCollider, false);
            _parentCollider = null;
        }
    }


    //this will have to be virtual/abstract to change interactions as required
    //if we want to keep the "pickup" name as we're using it for pickup detection, perhaps we can make a class instead
    //for example, col.gameObject.interaction(pickupType)
    //and have a public string pickupType at the start of this script
    private void OnCollisionEnter2D(Collision2D col)
    {
        ResetIgnoreCollisions();
        if (throwing == true)
        {
            InteractMonster(col);
        }
    }

    protected virtual void InteractMonster(Collision2D col)
    {
        if (col.gameObject.tag.Contains("Monster"))
        {
            Debug.Log("monsterName: " + col.gameObject.name + "\n thrower: " + thrower);
        }
    }





    /*
    void SetClosest()
    {
        GameObject obj = FindClosestPickup();
        if (Vector2.Distance(gameObject.transform.position,obj.transform.position) <= pickupRadius)
        {
            closestPickup = obj;
            Debug.Log(obj.name);
        }
        else
        {
            Debug.Log("none");
        }
    }

    public GameObject PickUp()
    {
        if (closestPickup != null)
        {
            closestPickup.tag = "Pickedup";
            pickedUp = closestPickup;
            closestPickup = FindClosestPickup();
            Debug.Log("picked up " + pickedUp.name);
            return pickedUp;
        }
        return null;
    }

    public GameObject Throw(Vector2 aim)
    {
        if (pickedUp != null)
        {
            pickedUp.tag = "Thrown";
            return pickedUp;
        }
        return null;
    }
    */
}
