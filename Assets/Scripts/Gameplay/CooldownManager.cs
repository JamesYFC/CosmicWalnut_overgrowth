﻿using UnityEngine;
using System.Collections;

public class CooldownManager : MonoBehaviour {

    private float cooldownStart;
    public float cooldownTime;
    private bool first = true;

    public void CheckFirst()
    {
        if (first)
        {
            //Debug.Log("first is true!");
            first = false;
        }
    }

    public bool GetFirstStatus()
    {
        return first;
    }

    public void SetCooldown()
    {
        cooldownStart = Time.time;
    }

    public bool CooldownCheck()
    {
        if (!first)
        {
            if (Time.time >= cooldownStart + cooldownTime)
            {
                //Debug.Log("not on cooldown...");
                return true;
            }
            else
            {
                //Debug.Log("still on cooldown! Time left: " + System.Math.Round(CooldownRemaining()).ToString());
                return false;
            }
        }
        else
        {
            //Debug.Log("not on cooldown... (first is true)");
            return true;
        }

    }

    public float CooldownRemaining()
    {
        if (Time.time < cooldownStart + cooldownTime)
        {
            return ((cooldownStart + cooldownTime) - Time.time);
        }
        else
        {
            return -1;
        }

    }
}
