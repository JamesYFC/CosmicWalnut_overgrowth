﻿using UnityEngine;
using System.Collections;

public abstract class Ability : MonoBehaviour {
    public GameObject caster;
    public float cooldownStart;
    public float cooldownTime;

    public abstract void Activate();

    public virtual void Cast()
    {
        if (CooldownCheck())
        {
            SetCooldown();
            Activate();
        }
    }
    public void SetCooldown()
    {
        cooldownStart = Time.time;
    }

    public bool CooldownCheck()
    {
        return (Time.time > cooldownStart + cooldownTime);
    }

    
}
