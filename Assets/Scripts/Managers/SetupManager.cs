﻿using UnityEngine;
using UnityEngine.SceneManagement;
using XInputDotNetPure;
using System.Collections;

public class SetupManager : MonoBehaviour
{
    private static bool[] _playerSetup = new bool[4];
    private static PlayerIndex[] playerIndices = new PlayerIndex[]{ PlayerIndex.One, PlayerIndex.Two, PlayerIndex.Three, PlayerIndex.Four };

    public static bool[] PLAYER_SETUP
    {
        get { return _playerSetup; }
    }



    [SerializeField]
    private PlayerSetupCard[] _setupCards = new PlayerSetupCard[0];

    [SerializeField]
    private string _gameSceneName = string.Empty;


    private bool _readyToPlay =false;




    public static int PlayerCount
    {
        get
        {
            int count = 0;
            for (int i = 0; i < _playerSetup.Length; i++)
            {
                count += _playerSetup[i] ? 1 : 0;
            }
            return count;
        }
    }

    public void BeginGame()
    {
        SceneManager.LoadScene(_gameSceneName);
    }



    // Use this for initialization
    void Start ()
    {
        for (int i = 0; i < _playerSetup.Length; i++)
        {
            _playerSetup[i] = false;
        } 
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (!_readyToPlay)
        {
            for (int i = 0; i < 4; i++)
            {                
                GamePadState state = GamePad.GetState(playerIndices[i]);
                if (state.Buttons.A == ButtonState.Pressed)
                {
                    _playerSetup[i] = true;
                    _setupCards[i].SetJoined();
                }
                
                if (state.Buttons.B == ButtonState.Pressed)
                {
                    _playerSetup[i] = false;
                    _setupCards[i].Deactivate();
                }

                if (state.Buttons.Start == ButtonState.Pressed)
                {
                    if (PlayerCount > 0)
                    {
                        _readyToPlay = true;
                        BeginGame();
                    }
                }
               
            }
        }

        



	}
}
