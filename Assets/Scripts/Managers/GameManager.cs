﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private Color[] _playerColors = new Color[] { Color.red, Color.blue, Color.yellow, Color.green };

    [SerializeField]
    private Transform[] _spawnPositions = new Transform[0];

    [SerializeField][Tooltip("Spawns a player if there are no players setup in SetupManager")]
    private bool _spawnDefaultPlayer = false;

    public GameObject[] allPlayers;

    [SerializeField]
    private UnityEvent _onPauseGame;
    [SerializeField]
    private UnityEvent _onResumeGame;

    //Is active game session happening
    private bool _gameActive = false;

    //Control whether or not the time scale is zero or not
    private bool _paused = false;

    [SerializeField]
    private GameObject _playerPrefab;




    public void PauseGame()
    {
        if (!_paused)
        {
            _paused = true;
            Time.timeScale = 0;
            _onPauseGame.Invoke();
        }
    }

    public void ResumeGame()
    {
        if (_paused)
        {
            _paused = false;
            Time.timeScale = 1f;
            _onResumeGame.Invoke();
        }
    }

    private void TogglePauseState()
    {
        if (_paused)
        {
            ResumeGame();
        }
        else
        {
            PauseGame();
        }
    }

    public void SpawnAllPlayers()
    {
        for (int i = 0; i < SetupManager.PLAYER_SETUP.Length; i++)
        {
            if (SetupManager.PLAYER_SETUP[i])
            {
                allPlayers[i].gameObject.SetActive(true);
            }
        }
    }

    private void SpawnPlayer(int playerIndex)
    {
       
        Vector3 spawnPosition = Vector3.zero;
        try
        {
            spawnPosition = _spawnPositions[playerIndex].position;
        }
        catch
        {

        }

        GameObject player = Instantiate(_playerPrefab, spawnPosition, Quaternion.identity) as GameObject;
        PlayerAnimator ani = player.GetComponent<PlayerAnimator>();
        if (ani != null)
        {
            ani.SetPlayerOutLineColour(_playerColors[playerIndex]);
        }

        AbstractInput input = player.GetComponent<AbstractInput>();
        if (input != null)
        {
            input.SetPlayerIndex(playerIndex);
        }
    }






	// Use this for initialization
	void Awake ()
    {
        SpawnAllPlayers();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetButtonDown("Escape"))
        {
            TogglePauseState();
        }
        if (Input.GetKeyDown("r"))
        {
            SceneManager.LoadScene("Scene_Jeremy");
        }

    }
}
