﻿using UnityEngine;
using System.Collections;

public abstract class AbstractInput : MonoBehaviour
{
    [SerializeField][Range(1,4)]
    private int _playerIndex = 1;

    public int PlayerIndex
    {
        get { return _playerIndex; }
    }

    public abstract void SetPlayerIndex(int index);

    protected Vector2 _movementVector;
    protected Vector2 _aimingVector;

    public Vector2 Movement
    {
        get { return _movementVector;}
    }

    public Vector2 AimingDirection
    {
        get { return _aimingVector; }
    }


    public abstract bool DashButton
    {
        get;
    }

    public abstract bool GrabButton
    {
        get;
    }





}
