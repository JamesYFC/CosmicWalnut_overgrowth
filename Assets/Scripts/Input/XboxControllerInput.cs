﻿using UnityEngine;
using XInputDotNetPure;
using System.Collections;
using System;

public class XboxControllerInput : AbstractInput
{
    public const float DEAD_ZONE = 0.15f;

    private GamePadState _currentState;
    private GamePadState _previousState;
    private XInputDotNetPure.PlayerIndex _gamePadIndex = XInputDotNetPure.PlayerIndex.One;

    
    public override void SetPlayerIndex(int index)
    {
        switch (index)
        {
            case 0:
            case 1:
            {
                    _gamePadIndex = XInputDotNetPure.PlayerIndex.One;
                    break;
            }
            case 2:
            {
                    _gamePadIndex = XInputDotNetPure.PlayerIndex.Two;
                    break;
            }
            case 3:
            {
                    _gamePadIndex = XInputDotNetPure.PlayerIndex.Three;
                    break;
            }
            case 4:
            {
                    _gamePadIndex = XInputDotNetPure.PlayerIndex.Four;
                    break;
            }
        }
    }

    public override bool DashButton
    {
        get
        {
            //return Input.GetKeyDown(_dashButton);
            return _currentState.Buttons.A == ButtonState.Pressed &&
                 _previousState.Buttons.A == ButtonState.Released;
        }
    }

    public override bool GrabButton
    {
        get
        {
            return _currentState.Buttons.X == ButtonState.Pressed &&
                _previousState.Buttons.X == ButtonState.Released;
            //return Input.GetKeyDown(_grabButton);
        }
    }



    void Start()
    {
        _currentState = GamePad.GetState(_gamePadIndex);
        _previousState = _currentState;
        SetPlayerIndex(PlayerIndex);
    }


    void Update ()
    {
        _previousState = _currentState;
        _currentState = GamePad.GetState(_gamePadIndex);

        _movementVector.x = _currentState.ThumbSticks.Left.X;
        _movementVector.y = _currentState.ThumbSticks.Left.Y;

        _aimingVector.x = _currentState.ThumbSticks.Right.X;
        _aimingVector.y = _currentState.ThumbSticks.Right.Y;



        for (int i = 0; i < 2; i++)
        {
            if (Mathf.Abs(_movementVector[i]) < DEAD_ZONE)
            {
                _movementVector[i] = 0;
            }
        }
    }
}
