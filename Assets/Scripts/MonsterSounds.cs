﻿using UnityEngine;
using System.Collections;

public class MonsterSounds : MonoBehaviour {
    public AudioClip[] sounds;
    private AudioSource audioPlayer;

	// Use this for initialization
	void Start () {
        audioPlayer = gameObject.GetComponent<AudioSource>();
	}
	
    public void PlayWindupSound()
    {
        audioPlayer.PlayOneShot(sounds[0]);
    }
    public void PlayPounceSound()
    {
        audioPlayer.PlayOneShot(sounds[1]);
    }
    public void PlayHitSound()
    {
        audioPlayer.PlayOneShot(sounds[2]);
    }
}
