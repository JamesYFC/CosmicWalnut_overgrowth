﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class TitleLogo : MonoBehaviour
{
    [SerializeField]
    private Color _startColor;
    [SerializeField]
    private Color _targetColor;

    [SerializeField]
    private Image _image;

    // Use this for initialization
    IEnumerator Start ()
    {
        _image.color = _startColor;
        float lerpTime = 1f;
        float timeElasped = 0;
        float t = 0;

        while (timeElasped < lerpTime)
        {
            t = timeElasped / lerpTime;
            _image.color = Color.Lerp(_startColor, _targetColor, t);
            yield return null;
            timeElasped += Time.deltaTime;
        }
        _image.color = _targetColor;

        yield return new WaitForSeconds(3.0f);

        timeElasped = 0;

        while (timeElasped < lerpTime)
        {
            t = timeElasped / lerpTime;
            _image.color = Color.Lerp(_targetColor, _startColor, t);
            yield return null;
            timeElasped += Time.deltaTime;
        }

        SceneManager.LoadScene(1);
    }
	

}
