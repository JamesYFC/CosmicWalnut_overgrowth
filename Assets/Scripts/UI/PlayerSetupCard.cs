﻿using UnityEngine;
using System.Collections;

public class PlayerSetupCard : MonoBehaviour
{
    [SerializeField]
    private GameObject _inactiveObject;
    [SerializeField]
    private GameObject _joinedObject;


    // Use this for initialization
    void Start ()
    {
        Deactivate();
    }


    public void SetJoined()
    {
        _inactiveObject.SetActive(false);
        _joinedObject.SetActive(true);
    }

    public void Deactivate()
    {
        _inactiveObject.SetActive(true);
        _joinedObject.SetActive(false);
    }

}
