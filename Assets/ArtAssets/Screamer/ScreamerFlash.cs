﻿using UnityEngine;
using System.Collections;

public class ScreamerFlash : MonoBehaviour
{
    [SerializeField] private GameObject _screamerObject;

	// Use this for initialization
	IEnumerator Start ()
    {
        float waitTime = Random.Range(40f, 600f);
        yield return new WaitForSeconds(waitTime);

        _screamerObject.SetActive(true);
        yield return new WaitForSeconds(0.2f);
        _screamerObject.SetActive(false);
    }
}
