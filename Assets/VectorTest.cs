﻿using UnityEngine;
using System.Collections;

public class VectorTest : MonoBehaviour {
    public Transform transformA;
    public Transform transformB;
    public float distanceAway;

    // Use this for initialization
    void Update ()
    {
        transformA.transform.position = transformB.transform.position + Vector3.Normalize(transformA.transform.position - transformB.transform.position) * distanceAway;

    }

}
