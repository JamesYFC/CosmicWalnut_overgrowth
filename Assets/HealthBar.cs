﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour {
    Player myPlayer;
    Slider healthBar;
    private float maxHealth;
    private float barHealth;
    private float interpol = 0;
    [SerializeField]
    private float duration;
    bool isLerping;

    // Use this for initialization
    void Start ()
    {
        myPlayer = gameObject.GetComponentInParent<Player>();
        healthBar = gameObject.GetComponentInChildren<Slider>();
        maxHealth = myPlayer.health.maxHealth;
        barHealth = maxHealth;
    }

    // Update is called once per frame
    void Update () {
        if (myPlayer.health.currentHealth < barHealth && !isLerping)
        {
            StartCoroutine(DoLerpStuff(barHealth, myPlayer.health.currentHealth));
        }
        else
        {
            interpol = 0;
        }
        healthBar.value = barHealth/ maxHealth;
    }

    private IEnumerator DoLerpStuff(float start, float end)
    {
        isLerping = true;
        for (float t = 0.0f; t < duration; t += Time.deltaTime)
        {
            barHealth = Mathf.Lerp(start, end, t / duration);
            yield return null;
        }
        isLerping = false;
        barHealth = end;
    }
}
