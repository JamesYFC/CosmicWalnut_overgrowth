﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CoolDownClock : MonoBehaviour {
    private Player myPlayer;
    public Image fill;
    public Image hand;
    private Canvas thisCanvas;

    // Use this for initialization
    void Start () {
        thisCanvas = gameObject.GetComponent<Canvas>();
        myPlayer = gameObject.GetComponentInParent<Player>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(!myPlayer._dashCooldown.CooldownCheck())
        {
            fill.fillAmount = myPlayer._dashCooldown.CooldownRemaining() / myPlayer._dashCooldown.cooldownTime;
            hand.rectTransform.rotation = Quaternion.Euler(0, 0, 360 * fill.fillAmount);
            thisCanvas.enabled = true;
        }
        else
        {
            fill.fillAmount = 1;
            hand.rectTransform.rotation = Quaternion.Euler(0, 0, 360);
            thisCanvas.enabled = false;
        }
	}
}
