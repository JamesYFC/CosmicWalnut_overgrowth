﻿using UnityEngine;
using System.Collections;

public class InputTester : MonoBehaviour {

    public string _axisName = string.Empty;
    public float _value;


	
	// Update is called once per frame
	void Update ()
    {
        _value = Input.GetAxis(_axisName);
	}
}
